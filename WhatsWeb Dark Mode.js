// ==UserScript==
// @name         WhatsWeb Dark Mode
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Activates the dark mode in WhatsWeb
// @author       Faith79
// @match        https://web.whatsapp.com/
// @grant        none
// ==/UserScript==

(function() {
    document.getElementsByClassName("web")[0].className += " dark";
})();